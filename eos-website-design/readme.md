## Design System elements

All files related to the design of the website of the EOS Design System should be placed inside this folder, as well as all the components.

The folders explained:

- \_template: The XD template file with the main components.
- Assets: Our Logo, fonts, and other assets we need to have to design our pieces.
- Internal: All the design of internal pages.
- External: The components/elements that are ready for external presentation.
