## SUSE Design System Adobe Source File Repository

___This repository contains the adobe files used to create assets for Eos, the SUSE Design System.___

Check the wiki (https://gitlab.com/SUSE-UIUX/eos/wikis/home)
for information about this project as well as details concerning the versioning of files and naming conventions.

This repository is automatically mirrored to https://gitlab.com/SUSE-UIUX/eos-backup

### Special attributions

- <a href="https://www.freepik.com/psd/background">Background psd created by freepik - www.freepik.com</a>

### Legal
The trademarks, logos and service marks ("Marks") contained in this repository are the property of SUSE or other third parties. You are not permitted to use the Marks without the prior written consent of SUSE or such third party which may own the Marks. "SUSE" and the SUSE logo are trademarks of SUSE IP Development Limited or its subsidiaries or affiliates.

Trademarks (possibly) include:
* "openSUSE"
* openSUSE logo
* "SUSE"
* SUSE logo
* "YAST"
* Chameleon mark
* AutoBuild
* SUSE Studio
* SUSECON

All other trademarks, trade names, or company names referenced herein are used for identification only and are the property of their respective owners.

For more information please see: https://www.suse.com/de-de/company/legal/#trademarks
